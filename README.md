# LBS ESP32 Web Server

![Platform](https://img.shields.io/badge/Platform-ESP32-green?style=for-the-badge) ![Language](https://img.shields.io/badge/Language-C%2B%2B-blue?style=for-the-badge)

An ESP32 library for quickly starting a web server with an ESP32. You can easily add buttons that toggle pins.

## Installation

### Quick Start

1. Download the source code as a zip file from [GitLab](https://gitlab.com/lbskola/lbs-esp32-web-server/).
2. Extract the contents into `C:\Users\<your_username>\Documents\Arduino\libraries`.
3. Remove `readme.md` and `.gitignore`.

### Command Line

1. `cd "C:\Users\<your_username>\Documents\Arduino\libraries"`
2. `git clone "https://gitlab.com/lbskola/lbs-esp32-web-server/.git"`
3. `sudo rm readme.md` and `sudo rm .gitignore`

## Usage

```cpp
#include <LBSWebServer.h>

LBSWebServer webserver = LBSWebServer();

void setup()
{
  webserver.setup("ssid", "password");
  webserver.addButton(17);
  webserver.addButton(2, "Name");
}

void loop()
{
  webserver.listen();
}
```

### Adding a button

In your setup function, add:

```cpp
webserver.addButton(int pin);
```

## Documentation

### Classes

```cpp
class LBSWebServer
{
  public:
    LBSWebServer(void);
    void addButton(int);
    void setup(char*, char*);
    void listen();
};
```

#### Methods

##### Constructor

Creates an instance of WebServer.

```cpp
WebServer();
```

##### addButton

Adds a button to the web page.

```cpp
void addButton(int pin);
void addButton(int pin, char* name);
```

| Parameter | Type   | Description                             |
| --------- | ------ | --------------------------------------- |
| pin       | int    | The pin the button uses as output       |
| name      | char\* | The name the button displays (Optional) |

##### setup

Connect to wifi and start web server.

```cpp
void setup(char* ssid, char* password);
```

| Parameter | Type   | Description              |
| --------- | ------ | ------------------------ |
| ssid      | char\* | The name of the wifi     |
| password  | char\* | The password of the wifi |

##### listen

Listens to connections. Should be called in the loop.

```cpp
void listen();
```

## Credit

This library was made by [Leo Ericson](https://gitlab.com/Mrswe123) for LBS.
