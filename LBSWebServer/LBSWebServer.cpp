#include "LBSWebServer.h"
#include <WiFi.h>
#include <WiFiServer.h>
#include <String>

LBSWebServer::LBSWebServer(void)
{
  server = WiFiServer(80);
  name = "ESP32 Web Server";
}

LBSWebServer::LBSWebServer(char* _name)
{
  server = WiFiServer(80);
  name = _name;
}

void LBSWebServer::addButton(int _pin)
{
  buttons.push_back(WSButton(_pin, ""));
}

void LBSWebServer::addButton(int _pin, char* _name)
{
  buttons.push_back(WSButton(_pin, _name));
}


void LBSWebServer::setup(char* ssid, char* password) {
  Serial.begin(115200);
  
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("WiFi connected.");
  Serial.println("IP adress:");
  Serial.println(WiFi.localIP());
  server.begin();
}

void LBSWebServer::listen(void)
{
  WiFiClient client = server.available(); // Listen for incoming clients
  String header;

  if (client) // If a new client connects,
  {
    Serial.println("New Client.");
    String currentLine = ""; // make a String to hold incoming data from the client
    while (client.connected())
    {
      if (client.available())
      { // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        header += c;
        if (c == '\n') // if the byte is a newline character
        {
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (!(currentLine.length() == 0)) // if you got a newline, then clear currentLine
          {
            currentLine = "";
          } 
          else
          {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            for (WSButton &button : buttons)
            {
              if (header.indexOf("GET /" + String(button.pin) + "/on") >= 0)
              {
                Serial.println("GPIO" + String(button.pin) + " on");
                button.setState(true);
              }
              else if (header.indexOf("GET /" + String(button.pin) + "/off") >= 0)
              {
                Serial.println("GPIO " + String(button.pin) + " off");
                button.setState(false);
              }
            }

            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");

            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #af4c4c; border: none; color: white; padding: 16px 40px; transition: background-color 200ms ease;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".off { background-color: #4CAF50; }</style></head>");
            
            client.println("<body><h1>" + String(name) + "</h1>");

            for (WSButton button : buttons)
            {
              client.println("<p>GPIO " + String(button.pin) + " - State " + String(button.state) + "</p>"); 
              if (button.state) {
                client.println("<p><a href=\"/" + String(button.pin) + "/off\"><button class=\"button off\">" + (button.name == "" ? String(button.pin) : String(button.name)) + "</button></a></p>");
              } else {
                client.println("<p><a href=\"/" + String(button.pin) + "/on\"><button class=\"button\">" + (button.name == "" ? String(button.pin) : String(button.name)) + "</button></a></p>");
              }
            }
            

            client.println("</body></html>");
            client.println();

            break;
          }
        }
        else if (c != '\r') // if you got anything else but a carriage return character,
        {
          currentLine = c;      // add it to the end of the currentLine
        }
      }
    }

    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

WSButton::WSButton(int _pin)
{
  state = false;
  pin = _pin;

  pinMode(pin, OUTPUT);
}

WSButton::WSButton(int _pin, char* _name)
{
  state = false;
  pin = _pin;
  name = _name;

  pinMode(pin, OUTPUT);
}

void WSButton::setState(bool _state)
{
  state = _state;
  if (state)
  {
    digitalWrite(pin, HIGH);
  }
  else
  {
    digitalWrite(pin, LOW);
  }  
}
