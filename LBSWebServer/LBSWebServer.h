// ensure this library description is only included once
#ifndef LBSWebServer_h
#define LBSWebServer_h

#include <list>
#include <WiFiServer.h>

class WSButton
{
  public:
    WSButton(void) {};
    WSButton(int);
    WSButton(int, char*);
    void setState(bool);
    int pin;
    bool state;
    char* name;
};

class LBSWebServer
{
  public:
    LBSWebServer(void);
    LBSWebServer(char*);
    void addButton(int);
    void addButton(int, char*);
    void setup(char*, char*);
    void listen(void);

  private:
    std::list<WSButton> buttons;
    WiFiServer server;
    char* name;
};

#endif

